import * as dotenv from "dotenv";

await dotenv.config({
  export: true,
  // .env.exampleと、.env+通常の環境変数を比較して不足がないかチェック
  safe: true,
  example: "./.env.example",
  path: "./.env",
});

const config = Deno.env.toObject();

export const ENV = {
  DATABASE_HOST: config["DATABASE_HOST"],
  DATABASE_NAME: config["DATABASE_NAME"],
  DATABASE_USER: config["DATABASE_USER"],
  DATABASE_PASSWORD: config["DATABASE_PASSWORD"],
  DATABASE_PORT: config["DATABASE_PORT"],
  CRYPTO_SECRET: config["CRYPTO_SECRET"],
};
