import { Head } from "$fresh/runtime.ts";
import Layout from "../components/Layout.tsx";
import Counter from "../islands/Counter.tsx";
import type { Handlers, PageProps } from "$fresh/server.ts";
import { getDecryptedSessionCookie } from "../logics/cookie.ts";
import { ContextState, Data, FormProps } from "../interfaces.ts";
import Map from "../islands/Map.tsx";
import Tabs from "../components/Tabs.tsx";

export const handler: Handlers<any, ContextState> = {
  async GET(req, ctx) {
    const cookies = await getDecryptedSessionCookie(req);
    return ctx.render!({
      isAllowed: cookies?.userId === "deno",
      csrfToken: ctx.state.maskedCsrfToken,
    });
  },
};
function Login(props: FormProps) {
  return (
    <form method="post" action="/api/login">
      <input type="hidden" value={props.csrfToken} name="csrfToken" />
      <div class="form-outline mb-4">
        <label class="form-label" for="username">ログインID</label>
        <input
          type="username"
          name="username"
          class="form-control"
          id="username"
        />
      </div>

      <div class="form-outline mb-4">
        <label class="form-label" for="password">パスワード</label>
        <input
          type="password"
          id="password"
          name="password"
          class="form-control"
        />
      </div>

      <button class="btn btn-primary btn-block mb-4">
        Sign in
      </button>
    </form>
  );
}

export default function Home({ data }: PageProps<Data>) {
  return (
    <Layout>
      <Head>
        <title>Earth Deno</title>

        <link
          rel="stylesheet"
          href="https://esm.sh/leaflet@1.9.3/dist/leaflet.css"
          integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
          crossOrigin=""
        />
      </Head>
      <div>
        {!data.isAllowed ? <Login csrfToken={data.csrfToken} /> : (
          <div>
            <Tabs />
            <Map />
            <a href="/logout">ログアウト</a> <Counter start={3} />
          </div>
        )}
      </div>
    </Layout>
  );
}
