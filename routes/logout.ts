import { Handlers } from "$fresh/server.ts";
import { deleteSessionCookie } from "../logics/cookie.ts";

export const handler: Handlers = {
  GET(req) {
    const headers = deleteSessionCookie(req);
    headers.set("location", "/");
    return new Response(null, {
      status: 302,
      headers,
    });
  },
};
