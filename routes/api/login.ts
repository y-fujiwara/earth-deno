import { Handlers } from "$fresh/server.ts";
import { decode } from "base64";
import {
  buildSessionCookieHeader,
  getDecryptedSessionCookie,
} from "../../logics/cookie.ts";

const USERNAME = "deno";
const PASSWORD = "land";

export const handler: Handlers = {
  async POST(req) {
    const form = await req.formData();

    const maskedToken = form.get("csrfToken") || "";
    const cookie = await getDecryptedSessionCookie(req);
    const sessionToken = cookie?.["csrfToken"] as string;
    try {
      validCsrfToken(maskedToken as string, sessionToken);
    } catch (e) {
      console.error(e);
      return new Response(null, { status: 403 });
    }

    if (
      form.get("username") === USERNAME && form.get("password") === PASSWORD
    ) {
      const headers = await buildSessionCookieHeader(req, { userId: "deno" });
      headers.set("location", "/");
      return new Response(null, {
        status: 303,
        headers,
      });
    } else {
      return new Response(null, {
        status: 403,
      });
    }
  },
};

function validCsrfToken(maskedToken: string, sessionToken: string) {
  const unmaskToken = unmask(maskedToken);
  return compareToken(unmaskToken, sessionToken);
}

function unmask(maskedToken: string): Uint8Array {
  const maskedTokenBytes = decode(maskedToken);
  const oneTimePad = maskedTokenBytes.subarray(0, 32);
  const encCsrfToken = maskedTokenBytes.subarray(32);
  const ret = oneTimePad.map((b, idx) => {
    return encCsrfToken[idx] ^ b;
  });

  return ret;
}

function compareToken(unmaskBytes: Uint8Array, sessionToken: string) {
  const sessionTokenBytes = decode(sessionToken);

  if (unmaskBytes.byteLength !== sessionTokenBytes.byteLength) {
    throw new Error("CSRFトークンの長さが異なります。");
  }

  const ret = sessionTokenBytes.every((byte, idx) => {
    return !(byte ^ unmaskBytes[idx]);
  });

  if (!ret) {
    throw new Error("CSRFトークンが異なります。");
  }
}
