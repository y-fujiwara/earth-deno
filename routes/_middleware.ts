// routes/_middleware.ts
import { MiddlewareHandlerContext } from "$fresh/server.ts";
import { ContextState } from "../interfaces.ts";
import {
  buildSessionCookieHeader,
  getDecryptedSessionCookie,
} from "../logics/cookie.ts";
import { encode, decode } from "base64";

const AUTHENTICITY_TOKEN_LENGTH = 32;

export async function handler(
  req: Request,
  ctx: MiddlewareHandlerContext<ContextState>,
) {
  ctx.state.maskedCsrfToken = "";
  let resp: Response;
  const accept = req.headers.get("accept");

  if (/^GET$/.test(req.method) && /text\/html/.test(accept!)) {

    const cookie = await getDecryptedSessionCookie(req);
    let realToken = cookie?.csrfToken;
    let cookieHeaders = null;
    if (typeof realToken !== "string") {
      realToken = csrfToken();
      cookieHeaders = await buildSessionCookieHeader(req, {
        csrfToken: realToken,
      });
    }

    ctx.state.maskedCsrfToken = maskedToken(realToken as string);
    resp = await ctx.next();
    if (cookieHeaders) {
      resp.headers.set("Set-Cookie", cookieHeaders.get("Set-Cookie") as string);
    }
  } else {
    resp = await ctx.next();
  }


  return resp;
}

function csrfToken(): string {
  return encode(crypto.getRandomValues(new Uint8Array(AUTHENTICITY_TOKEN_LENGTH)));
}

function maskedToken(token: string): string {
  const bytes = crypto.getRandomValues(
    new Uint8Array(AUTHENTICITY_TOKEN_LENGTH),
  );
  const tokenBytes = decode(token);
  const ret = bytes.map((b, idx) => {
    return tokenBytes[idx] ^ b;
  });

  const maskedBytes = new Uint8Array(bytes.length + ret.length);
  maskedBytes.set(bytes);
  maskedBytes.set(ret, bytes.length);
  return encode(maskedBytes);
}
