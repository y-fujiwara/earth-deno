import {
  ClientPostgreSQL,
  NessieConfig,
} from "https://deno.land/x/nessie@2.0.0-rc2/mod.ts";
import { ENV } from "./env.ts";

/** Select one of the supported clients */
const client = new ClientPostgreSQL({
  database: ENV.DATABASE_NAME,
  hostname: ENV.DATABASE_HOST,
  port: ENV.DATABASE_PORT,
  user: ENV.DATABASE_USER,
  password: ENV.DATABASE_PASSWORD,
});

/** This is the final config object */
const config: NessieConfig = {
  client,
  migrationFolders: ["./db/migrations"],
  seedFolders: ["./db/seeds"],
};

// RUN MIGATE: deno run -A --import-map=import_map.json --unstable https://deno.land/x/nessie/cli.ts make:migration create_users
export default config;
