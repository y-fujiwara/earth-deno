import { deleteCookie, getCookies, setCookie } from "std/http/cookie.ts";
import { tokenDecrypt, tokenEncrypt } from "./encrypt.ts";

export const SESSION_COOKIE_NAME = "earth-deno-session";

export async function buildSessionCookieHeader(
  req: Request,
  value: Record<string, unknown>,
): Promise<Headers> {
  const header = new Headers();
  const url = new URL(req.url);

  const cookies = await getDecryptedSessionCookie(req);
  const mergeCookies = { ...cookies, ...value };
  const encryptedValue = await tokenEncrypt(JSON.stringify(mergeCookies));

  setCookie(header, {
    name: SESSION_COOKIE_NAME,
    value: encryptedValue,
    maxAge: 120,
    sameSite: "Lax", // this is important to prevent CSRF attacks
    domain: url.hostname,
    path: "/",
    secure: true,
  });

  return header;
}

export async function getDecryptedSessionCookie(
  req: Request,
): Promise<Record<string, unknown> | null> {
  const encCookies = getCookies(req.headers)[SESSION_COOKIE_NAME];
  if (encCookies) {
    return JSON.parse(await tokenDecrypt(encCookies)) as Record<
      string,
      unknown
    >;
  }

  return null;
}

export function deleteSessionCookie(req: Request): Headers {
  const url = new URL(req.url);
  const headers = new Headers(req.headers);
  deleteCookie(headers, SESSION_COOKIE_NAME, {
    path: "/",
    domain: url.hostname,
  });
  return headers;
}
