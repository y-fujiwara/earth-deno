import { decode, encode } from "base64";
import { ENV } from "../env.ts";

export async function tokenEncrypt(val: string): Promise<string> {
  const ivBytes = iv();
  const c = await crypto.subtle.encrypt(
    { name: "AES-GCM", iv: ivBytes },
    await loadSecret(),
    new TextEncoder().encode(val),
  );

  return `${encode(new Uint8Array(ivBytes))}--${
    encode(new Uint8Array(c))
  }`;
}

export async function tokenDecrypt(val: string): Promise<string> {
  const [ivVal, token] = val.split("--");
  const encryptedData = decode(token);
  const ivData = decode(ivVal);

  const decryptedArrayBuffer = await crypto.subtle.decrypt(
    { name: "AES-GCM", iv: ivData },
    await loadSecret(),
    encryptedData,
  );
  return new TextDecoder().decode(new Uint8Array(decryptedArrayBuffer));
}

function iv(): Uint8Array {
  // TODO: getRandomValuesは正直微妙だけどちゃんと一意にするにはDB関連がないと厳しい
  return crypto.getRandomValues(new Uint8Array(12));
}

async function loadSecret() {
  let key = null;
  try {
    key = await Deno.readTextFile("secret.json");
  } catch (e) {
    console.error(e);
    console.log("try read key from env.");
    key = ENV.CRYPTO_SECRET;
  }

  const text = JSON.parse(key);
  return await crypto.subtle.importKey(
    "jwk",
    text,
    { name: "AES-GCM" },
    false,
    ["encrypt", "decrypt"],
  );
}
