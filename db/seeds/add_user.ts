import {
  AbstractSeed,
  ClientPostgreSQL,
  Info,
} from "https://deno.land/x/nessie@2.0.0-rc2/mod.ts";
import * as bcrypt from "bcrypt";

/*
        id SERIAL PRIMARY KEY,
        email character varying not null UNIQUE,
        login_name CHARACTER VARYING not null UNIQUE,
        show_name character VARYING not null,
        password character varying not null,
        bookmark character varying,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp not null default current_timestamp

*/
export default class extends AbstractSeed<ClientPostgreSQL> {
  async run(_: Info): Promise<void> {
    const loginName = "deno";
    const email = "sample@example.com";
    const showName = "test";
    const salt = await bcrypt.genSalt(12);
    const password = await bcrypt.hash("land", salt);

    await this.client.queryArray(
      "INSERT INTO users (email, login_name, show_name, password) VALUES ($1, $2, $3, $4);",
      email,
      loginName,
      showName,
      password,
    );
  }
}
