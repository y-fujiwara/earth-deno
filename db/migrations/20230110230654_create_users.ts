import {
  AbstractMigration,
  ClientPostgreSQL,
  Info,
} from "https://deno.land/x/nessie@2.0.0-rc2/mod.ts";

export default class extends AbstractMigration<ClientPostgreSQL> {
  /** Runs on migrate */
  async up(info: Info): Promise<void> {
    await this.client.queryArray(`
      CREATE TABLE IF NOT EXISTS users(
        id SERIAL PRIMARY KEY,
        email character varying not null UNIQUE,
        login_name CHARACTER VARYING not null UNIQUE,
        show_name character VARYING not null,
        password character varying not null,
        bookmark character varying,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp not null default current_timestamp
      )
    `);
    await this.client.queryArray(
      `CREATE INDEX user_email_idx on users(email)`,
    );
    await this.client.queryArray(
      `CREATE INDEX user_login_idx on users(login_name)`,
    );
  }

  /** Runs on rollback */
  async down(info: Info): Promise<void> {
    await this.client.queryArray("DROP INDEX IF EXSISTS user_email_idx");
    await this.client.queryArray("DROP INDEX IF EXSISTS user_login_idx");
    await this.client.queryArray("DROP SEQUENCE users_id_seq");
    await this.client.queryArray("DROP TABLE users");
  }
}
