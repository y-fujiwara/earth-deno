import { Head } from "$fresh/runtime.ts";
import { BaseProps } from "../interfaces.ts";

export default function Layout({ children }: BaseProps) {
  return (
    <>
      <div class="container-xxl bd-gutter mt-3 my-md-4 bd-layout">
        {children}
      </div>
    </>
  );
}
