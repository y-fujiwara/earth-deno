import { useEffect, useRef, useState } from "preact/hooks";

export function MapContainer() {
  const mapContainer = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<L.Map | null>(null);
  useEffect(() => {
    if (typeof document === "object" && mapContainer && mapContainer.current) {
      import("leaflet").then((L) => {
        const m = L.map("map").setView([51.505, -0.09], 13);
        L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
          maxZoom: 19,
          attribution:
            '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        }).addTo(m);
        setMap(map);
      });
    }
  }, []);

  return <div id="map" style={{ height: "180px" }} ref={mapContainer}></div>;
}
