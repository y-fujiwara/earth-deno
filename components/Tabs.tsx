export default function Tabs() {
  return (
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">Map</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Chat</a>
      </li>
      <li class="nav-item">
        <a
          class="nav-link"
          href="/logout"
        >
          ログアウト
        </a>
      </li>
    </ul>
  );
}
