import { ComponentChildren } from "preact";

export interface BaseProps {
  children: ComponentChildren;
}

export interface ContextState {
  maskedCsrfToken?: string;
}

export interface Data {
  isAllowed: boolean;
  csrfToken: string;
}

export interface FormProps {
  csrfToken: string;
}
